import React from 'react'
import Navigation from '../components/Navigation'
import { Outlet } from "react-router-dom";

function MainLayout() {
  return (
    <>
      <header>
        <h1>Harry Potter Fansite</h1>
        <Navigation />
      </header>
      <main>
        {/* ici je veux afficher le contenu des différentes pages */}
        <Outlet />
      </main>
      <footer>
        ©2023 Hermione
      </footer>
    </>
  )
}

export default MainLayout