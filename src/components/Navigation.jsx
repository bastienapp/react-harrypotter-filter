import React from 'react'
import { Link } from 'react-router-dom'

/*
NavLink permet de savoir qu'elle est la page actuellement active (par exemple pour faire du css sur le lien de la page en cours)
*/
function Navigation() {
  return (
    <nav>
      <ul>
        <li>
          {/* Faire un Link changer de page sans recharger le navigateur */}
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/characters">Character List</Link>
        </li>
        <li>
          <Link to="/newsletter">Newsletter</Link>
        </li>
      </ul>
    </nav>
  )
}

export default Navigation