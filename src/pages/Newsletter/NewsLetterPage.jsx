function NewsLetterPage() {
  return (
    <div>
      <h3>Subscribe to our newsletter:</h3>
      <form>
        <label>
          Email:
          <input type="email" placeholder="Your email here" />
        </label>
        <button type="submit">Send</button>
      </form>
    </div>
  );
}

export default NewsLetterPage;
