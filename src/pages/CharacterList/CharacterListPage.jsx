import React, { useEffect, useState } from "react";
import CharacterList from "./CharacterList";
import axios from "axios";

function CharacterListPage() {
  const [characterList, setCharacterList] = useState([]);

  useEffect(() => {
    axios
      .get("https://hp-api.onrender.com/api/characters")
      .then((response) => setCharacterList(response.data));
  }, []);

  return (
    <>
      <CharacterList characterList={characterList} />
    </>
  );
}

export default CharacterListPage;
