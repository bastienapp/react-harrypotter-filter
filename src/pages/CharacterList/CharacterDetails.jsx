function CharacterDetails(props) {
  const { name, actor, house } = props;

  return (
    <div>
      <h3>{name}</h3>
      <h4>{actor}</h4>
      <h5>{house}</h5>
    </div>
  );
}

export default CharacterDetails;
