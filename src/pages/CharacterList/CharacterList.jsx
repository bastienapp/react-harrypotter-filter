import { useState } from "react";
import CharacterDetails from "./CharacterDetails";

function CharacterList(props) {
  const { characterList } = props;

  const [selectedHouse, setSelectedHouse] = useState("");

  function selectHouse(house) {
    setSelectedHouse(house);
  }

  return (
    <div>
      <section>
        <button onClick={() => selectHouse("")}>All</button>
        <button onClick={() => selectHouse("Gryffindor")}>Gryffindor</button>
        <button onClick={() => selectHouse("Slytherin")}>Slytherin</button>
        <button onClick={() => selectHouse("Ravenclaw")}>Ravenclaw</button>
        <button onClick={() => selectHouse("Hufflepuff")}>Hufflepuff</button>
      </section>
      {characterList
        .filter((eachCharacter) =>
          selectedHouse !== "" ? eachCharacter.house === selectedHouse : true
        )
        /*
        // le même filtre sous forme de condition
        .filter((eachCharacter) => {
          if (selectedHouse !== "") {
            return eachCharacter.house === selectedHouse;
          } else {
            return true;
          }
        })
        */
        .map((eachCharacter) => (
          <CharacterDetails
            key={eachCharacter.id}
            name={eachCharacter.name}
            actor={eachCharacter.actor}
            house={eachCharacter.house}
          />
        ))}
    </div>
  );
}

export default CharacterList;
