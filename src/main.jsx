import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import HomePage from "./pages/Home/HomePage.jsx";
import NewsLetterPage from "./pages/Newsletter/NewsLetterPage.jsx";
import MainLayout from "./layouts/MainLayout.jsx";
import CharacterListPage from "./pages/CharacterList/CharacterListPage.jsx";

const router = createBrowserRouter(
  // tableau des routes de mon application
  [
    /* la page d'acceuil */
    {
      path: "/", // page page défaut
      element: <MainLayout />,
      children: [
        {
          path: "/",
          element: <HomePage />,
        },
        /* la liste des personnes */
        {
          path: "/characters",
          element: <CharacterListPage />,
        },
        /* une page de contact */
        {
          path: "/newsletter", // page page défaut
          element: <NewsLetterPage />,
        },
      ],
    },
  ]
);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* je donne le routeur à mon application */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
